(function(){

  var app = angular.module('notesApp',['ngRoute', 'ngMaterial']);

  app.config(['$locationProvider', '$routeProvider',
      function ($locationProvider, $routeProvider) {

        $routeProvider
          .when('/', {
            templateUrl: '/partials/notes-view.html',
            controller: 'notesController'
          })
          .when('/login', {
             templateUrl: '/partials/login.html',
             controller: 'loginController',
          })
          .when('/logout', {
             templateUrl: '',
             controller: 'logoutController',
          })
          .otherwise('/');
      }
  ]);

  app.run(['$rootScope', '$location', 'AuthService', function ($rootScope, $location, AuthService) {
      $rootScope.$on('$routeChangeStart', function (event) {

          if ($location.path() == "/login"){
             return;
          }

          if (!AuthService.isLoggedIn()) {
              console.log('DENY');
              event.preventDefault();
              $location.path('/login');
          }
      });
  }]);


  app.service('AuthService', function($http){
        var loggedUser = null;

        function login (username, password){
            return $http.post("api/login", {username: username, password: password}).then(function(user){
                loggedUser = user;
                localStorage.setItem('user', user);
                return user;
            }, function(error){
                loggedUser = null;
            })
        }

        function logout(){
            return $http.post("api/logout").then(function(response){
                localStorage.clear();
                return response;
            }, function(error){
            })
        }

        function isLoggedIn(){
            return localStorage.getItem('user') != null;
        }
        return {
            login : login,
            isLoggedIn: isLoggedIn,
            logout: logout
        }
  });

  app.service('NoteService', function($http){

      function findAll (){
          return $http.get("api/notes/findAll").then(function(response){
            return response;
          }, function(error){
             alert("Error happened during fetch data");
          })
      }

      function findById (id){
           return $http.get("api/notes/findById/"+id).then(function(response){
                return response;
           }, function(error){
             alert("Error happened during fetch data");
           })
      }

      function save (note){
           return $http.post("api/notes/createOrUpdate", note).then(function(response){
                return response;
           }, function(error){
                alert("Error happened during save note");
           })
      }

      function deleteNotes (id){
           return $http.delete("api/notes/delete/" + id).then(function(response){
                return response;
           }, function(error){
                alert("Error happened during delete note");
           })
      }

      return {
        findAll: findAll,
        findById: findById,
        save: save,
        deleteNotes: deleteNotes
      }
    });

  app.controller('loginController', function($scope, AuthService, $location){

    $scope.login = {
        username : null,
        password : null
    };

    $scope.login = function(form){
        $scope.invalidCreds = false;
        $scope.emptyFields = false;
        if(form.$valid) {
            AuthService.login($scope.login.username, $scope.login.password).then(function(user){
                if(user == null) {
                    $scope.invalidCreds = true;
                } else {
                    $location.path("/");
                }
            }, function(error){
                console.log(error);
                $scope.invalidCreds = true;
            });
        } else {
            $scope.emptyFields = true;
        }
    };
  });

  app.controller('logoutController', function($scope, AuthService, $location){
     $scope.logout = function(){
          AuthService.logout().then(function(response) {
               $location.path("/login");
          }, function(error){
               console.log(error);
          });
      };

      $scope.isLoggedIn = function() {
          return AuthService.isLoggedIn();
      }
    });


  app.controller('notesController', function($scope, NoteService){

    $scope.isNotSavedNote = null;
    $scope.isEditCreateView = false;
    $scope.isFormInvalid = false;
    $scope.note = {
        name: null,
        content: null
    };

    $scope.newNoteView = function(){
        $scope.isEditCreateView = true;
        $scope.isNotSavedNote = null;
        $scope.note = {
            name: null,
            content: null
        };
    };

    $scope.deleteNote = function (id) {
        NoteService.deleteNotes(id).then(function(response){
            if(response.status == 200 && response.data) {
                $scope.isEditCreateView = false;
                $scope.note = {};
                $scope.loadNotes();
            } else {
                $scope.isNotDeletedNote = true;
            }
        }, function(error){
            $scope.isNotDeletedNote = true;
        });
    };

    $scope.viewNote = function(id){
        $scope.isNotSavedNote = null;
        NoteService.findById(id).then(function(response){
            if(response.status == 200 && response.data != null) {
                $scope.note = response.data;
                $scope.isEditCreateView = true;
            } else {
                $scope.isNoteNotFound = true;
            }
        }, function(error){
            $scope.isNoteNotFound = true;
        });
    }

    $scope.loadNotes = function(){
        NoteService.findAll().then(function(response){
            if(response.status == 200 && response.data != null) {
                $scope.noteList = response.data;
            }
        }, function(error){
            console.log(error);
        });
    }

    $scope.saveNote = function(form){
        if(form.$valid) {
            console.log($scope.note);
            NoteService.save($scope.note).then(function(response){
                if(response.status == 201 && response.data) {
                    $scope.loadNotes();
                    $scope.isNotSavedNote = false;
                } else {
                    $scope.isNotSavedNote = true;
                }
            }, function(error){
                $scope.isNotSavedNote = true;
            });
        } else {
            $scope.isFormInvalid = true;
        }
    }

    $scope.cancelNote = function(){
        $scope.isEditCreateView = false;
        $scope.isFormInvalid = false;
        $scope.isNotSavedNote = null;
        $scope.note = {};
    }
  });

  app.directive('confirm', [function () {
          return {
              priority: 100,
              restrict: 'A',
              link: {
                  pre: function (scope, element, attrs) {
                      var msg = attrs.confirm;

                      element.bind('click', function (event) {
                          if (!confirm(msg)) {
                              event.stopImmediatePropagation();
                              event.preventDefault;
                          }
                      });
                  }
              }
          };
      }]);
})();