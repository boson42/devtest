package com.thevirtugroup.postitnote.model;

import java.util.Date;

/**
 * Created by boson on 29.01.2017.
 */
public class Note {

    private Long id;
    private String name;
    private String content;
    private Date date;

    public Note(){}

    public Note(Long id, String name, String content, Date date) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
