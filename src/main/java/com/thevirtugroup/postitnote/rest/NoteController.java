package com.thevirtugroup.postitnote.rest;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 */
@RestController
@RequestMapping("/api/notes")
public class NoteController {
    @Autowired
    private NoteRepository noteRepository;

    @RequestMapping(path = "findAll", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<List<Note>> findAll() {
        List<Note> notes = noteRepository.findAll();
        if(notes != null && notes.size() > 0){
            return new ResponseEntity<>(notes, HttpStatus.OK);
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "findById/{id}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<Note> findById(@PathVariable Long id) {
        Note note = noteRepository.findById(id);
        if(note != null) {
            return new ResponseEntity<>(note, HttpStatus.OK);
        }
        return new ResponseEntity<>(new Note(), HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "createOrUpdate", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<Boolean> createOrUpdate(@RequestBody Note note) {
        if(noteRepository.findById(note.getId()) != null) {
            boolean updated = noteRepository.update(note.getId(), note);
            return new ResponseEntity<>(updated, updated ? HttpStatus.CREATED : HttpStatus.NOT_MODIFIED);
        }
        boolean created = noteRepository.create(note);
        return new ResponseEntity<>(created, created ? HttpStatus.CREATED : HttpStatus.NOT_MODIFIED);
    }

    @RequestMapping(path = "delete/{id}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity<Boolean> delete(@PathVariable Long id) {
        boolean deleted = noteRepository.delete(id);
        return new ResponseEntity<>(deleted, deleted ? HttpStatus.OK : HttpStatus.NOT_MODIFIED);
    }
}
