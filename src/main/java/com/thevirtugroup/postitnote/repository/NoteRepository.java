package com.thevirtugroup.postitnote.repository;

import com.thevirtugroup.postitnote.model.Note;

import java.util.List;

/**
 * Created by seriksan on 29.01.2017.
 */
public interface NoteRepository {

    List<Note> findAll();

    Note findById(Long id);

    boolean create(Note note);

    boolean update(Long id, Note note);

    boolean delete(Long id);
}
