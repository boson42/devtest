package com.thevirtugroup.postitnote.repository;


import com.thevirtugroup.postitnote.model.User;

public interface UserRepository {

    User findUserByUsername(String username);

    User findById(Long id);

}
