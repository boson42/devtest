package com.thevirtugroup.postitnote.repository.impl;

import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by seriksan on 29.01.2017.
 */
@Repository
public class NoteRepositoryImpl implements NoteRepository {

    private Map<Long, Note> noteMap = new HashMap<>();
    private static Long SEQUENCE = 10L;

    public NoteRepositoryImpl() {
        for (Long i = 0L; i < 10L; i++) {
            noteMap.put(i, new Note(i, "TestName" + i, "TestContent" + i, new Date()));
        }
    }

    @Override
    public List<Note> findAll() {
        return new ArrayList<>(noteMap.values());
    }

    @Override
    public Note findById(Long id) {
        return noteMap.get(id);
    }

    @Override
    public boolean create(Note note) {
        Long id = SEQUENCE++;
        note.setId(id);
        note.setDate(new Date());
        noteMap.put(id, note);
        return true;
    }

    @Override
    public boolean update(Long id, Note note) {
        if(noteMap.get(id) != null) {
            noteMap.get(id).setName(note.getName());
            noteMap.get(id).setContent(note.getContent());
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Long id) {
        Note deletedNote = noteMap.remove(id);
        return deletedNote != null;
    }
}
