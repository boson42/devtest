package com.thevirtugroup.postitnote.repository.impl;


import com.thevirtugroup.postitnote.model.User;
import com.thevirtugroup.postitnote.repository.UserRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private User defaultUser;

    public UserRepositoryImpl() {
        defaultUser = new User();
        defaultUser.setId(999L);
        defaultUser.setName("Johnny Tango");
        defaultUser.setPassword("password");
        defaultUser.setUsername("user");
    }

    @Override
    public User findUserByUsername(String username){
        if ("user".equals(username)){
            return defaultUser;
        }
        return null;
    }

    @Override
    public User findById(Long id){
        if (defaultUser.getId().equals(id)){
            return defaultUser;
        }
        return null;
    }


}
