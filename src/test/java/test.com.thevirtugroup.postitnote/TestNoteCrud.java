package test.com.thevirtugroup.postitnote;

import com.thevirtugroup.postitnote.Application;
import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by seriksan on 29.01.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class TestNoteCrud {

    @Autowired
    private NoteRepository noteRepositoryMock;
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
    private Map<Long, Note> noteMap = new HashMap<>();
    private Note createdNote = new Note(11L, "Test11", "Test11", new Date());

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testFindAll() throws Exception {
        mockMvc.perform(get("/api/notes/findAll")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)));
    }

    @Test
    public void testFindById() throws Exception {
        mockMvc.perform(get("/api/notes/findById/" + 2L)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("TestName2")))
                .andExpect(jsonPath("$.content", is("TestContent2")));
    }

    @Test
    public void testFindByIdError() throws Exception {
        mockMvc.perform(get("/api/notes/findById/" + 12L)
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreate() throws Exception {
        mockMvc.perform(post("/api/notes/createOrUpdate")
                .content(json("createNote.json"))
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", is(true)));
    }

    @Test
    public void testUpdate() throws Exception {
        mockMvc.perform(post("/api/notes/createOrUpdate")
                .content(json("updateNote.json"))
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", is(true)));
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(delete("/api/notes/delete/" + 2L)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(true)));
    }

    @Test
    public void testDeleteError() throws Exception {
        mockMvc.perform(delete("/api/notes/delete/" + 12L)
                .contentType(contentType))
                .andExpect(status().isNotModified())
                .andExpect(jsonPath("$", is(false)));
    }

    private String json(String fileName) {
        Scanner scanner = new Scanner(this.getClass().getResourceAsStream(fileName)).useDelimiter("\\A");
        return scanner.next();
    }
}
