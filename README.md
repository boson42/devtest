## Explanation

Just wanted to clarify and comment some moments about this application for potential recruiter.

## Testing

I was thinking if I need to test whole flow of CRUD operations or mock the repository. So, I decided to choose 1st option.
If I would mock repo, there would be no meaning to write the tests.
Of course, sometimes it's better to mock the stuff to be environment independent.
Also, I was thinking about testing LogIn logic, but then realized that it's done through Spring Security and tested better than I can even imagine.
And last thing is about AngularJS testing. I just had no time to do it. I wanted to spend one more hour for it, but it's too late, so maybe next time.

## Recommendation

I guess there's no meaning to use UserRepo for log in purposes. For that case(we have no database), it'd be better just to use in memory authentication.
On the other hand, it would be good to use in memory database for this task(e.g. hsqldb). It'll give an opportunity to check if developer is able to work with databases and can be easily used in JUnit tests.
